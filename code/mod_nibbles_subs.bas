
'Renamed snake sammy to pete

'Added function AvoidWalls for computer controlled snake
FUNCTION AvoidWalls (SnakeRow, SnakeCol, SnakeDir, NumberRow, NumberCol, WallInTheWay)
   WallInTheWay = FALSE

   SELECT CASE SnakeDir
   CASE UP
      IF PointIsThere(SnakeRow - 1, SnakeCol, colorTable(4)) THEN
         WallInTheWay = TRUE
         IF NumberCol > SnakeCol THEN
            SnakeDir = RIGHT
            IF PointIsThere(SnakeRow, SnakeCol + 1, colorTable(4)) THEN
               AvoidWalls = LEFT
            END IF
         ELSE
            AvoidWalls = LEFT
            IF PointIsThere(SnakeRow, SnakeCol - 1, colorTable(4)) THEN
               AvoidWalls = RIGHT
            END IF
         END IF
      END IF
   CASE DOWN
      IF PointIsThere(SnakeRow + 1, SnakeCol, colorTable(4)) THEN
         WallInTheWay = TRUE
         IF SnakeCol < NumberCol THEN
            AvoidWalls = RIGHT
            IF PointIsThere(SnakeRow, SnakeCol + 1, colorTable(4)) THEN
               AvoidWalls = LEFT
            END IF
         ELSE
            AvoidWalls = LEFT
            IF PointIsThere(SnakeRow, SnakeCol - 1, colorTable(4)) THEN
               AvoidWalls = RIGHT
            END IF
         END IF
      END IF
   CASE LEFT
      IF PointIsThere(SnakeRow, SnakeCol - 1, colorTable(4)) THEN
         WallInTheWay = TRUE
         IF NumberRow < SnakeRow THEN
            AvoidWalls = UP
            IF PointIsThere(SnakeRow - 1, SnakeCol, colorTable(4)) THEN
               AvoidWalls = DOWN
            END IF
         ELSE
            AvoidWalls = DOWN
            IF PointIsThere(SnakeRow + 1, SnakeCol, colorTable(4)) THEN
               AvoidWalls = UP
            END IF
         END IF
      END IF
   CASE RIGHT
      IF PointIsThere(SnakeRow, SnakeCol + 1, colorTable(4)) THEN
         WallInTheWay = TRUE
         IF NumberRow > SnakeRow THEN
            AvoidWalls = DOWN
            IF PointIsThere(SnakeRow + 1, SnakeCol, colorTable(4)) THEN
               AvoidWalls = UP
            END IF
         ELSE
            AvoidWalls = UP
            IF PointIsThere(SnakeRow - 1, SnakeCol, colorTable(4)) THEN
               AvoidWalls = DOWN
            END IF
         END IF
      END IF
   END SELECT
END FUNCTION


'Significantly modified GetInputs
SUB GetInputs (NumPlayers, speed, diff$, BeginLives, StartLevel, Controllers(), Demo)
    COLOR 7, 0
    CLS

    LOCATE 3, 15: PRINT "Select one of the following for your inputs :"
    LOCATE 5, 1: PRINT "1. Standard: 1 human player, 0 computer players, speed 1, Don't increase speed,"
    LOCATE 6, 10: PRINT "   5 lives, and start at level 1"
    LOCATE 8, 1: PRINT "2. Hard: 1 human player, 0 computer players, speed 100, Do increase speed,"
    LOCATE 9, 10: PRINT "   15 lives, and start at level 1"
    LOCATE 11, 1: PRINT "3. Action: 2 human players, 1 computer player, speed 100, Don't increase speed,"
    LOCATE 12, 10: PRINT "   15 lives, and start at level 1"
    LOCATE 14, 1: PRINT "4. Demo: 0 human players, 1 computer player, speed 50, Do increase speed, "
    LOCATE 15, 10: PRINT "1 life, start at level 1"
    LOCATE 17, 1: PRINT "5. Custom"
    LOCATE 18, 15: INPUT "Enter in one of the following numbers"; GameNum$

    SELECT CASE GameNum$
    CASE "1"
      NumPlayers = 1
      Controllers(1) = FALSE
      speed = (100 - 1) * 2 + 1
      diff$ = "N"
      BeginLives = 5
      StartLevel = 1
      CLS
      EXIT SUB
    CASE "2"
      NumPlayers = 1
      Controllers(1) = FALSE
      speed = (100 - 100) * 2 + 1
      diff$ = "Y"
      BeginLives = 15
      StartLevel = 1
      CLS
      EXIT SUB
    CASE "3"
      NumPlayers = 3
      Controllers(1) = FALSE
      Controllers(2) = FALSE
      Controllers(3) = TRUE
      speed = (100 - 100) * 2 + 1
      diff$ = "N"
      BeginLives = 15
      StartLevel = 1
      CLS
      EXIT SUB
    CASE "4":
      NumPlayers = 1
      Controllers(1) = TRUE
      speed = (100 - 50) * 2 + 1
      diff$ = "Y"
      BeginLives = 1
      StartLevel = 1
      CLS
      EXIT SUB
    CASE ELSE
      CLS
    END SELECT

    DO
      LOCATE 4, 15
      INPUT "How many human players do you want in the game (0 to 2)"; Humans$

      IF Humans$ = "0" THEN
         Demo = TRUE
         EXIT DO
      ELSE
         IF VAL(Humans$) >= 1 AND VAL(Humans$) <= 2 THEN
            Demo = FALSE
            EXIT DO
         END IF
      END IF
      LOCATE 4, 15: PRINT SPACE$(50)
    LOOP

    FOR I = 1 TO VAL(Humans$)
      Controllers(I) = FALSE
    NEXT

    DO
      LOCATE 5, 13: PRINT SPACE$(80)
      LOCATE 6, 13
      PRINT "How many computer players do you want in the game (0 to"
      LOCATE 6, 68: PRINT STR$(MAXPLAYERS - VAL(Humans$))
      LOCATE 6, 70: PRINT ")"
      LOCATE 6, 72
      INPUT ""; Computers$

      IF Computers$ = "0" THEN
         EXIT DO
      ELSE
         IF VAL(Computers$) >= 1 AND VAL(Computers$) <= MAXPLAYERS - VAL(Humans$) THEN
            EXIT DO
         END IF
      END IF
    LOOP

    FOR I = 1 TO VAL(Computers$)
      Controllers(I + VAL(Humans$)) = TRUE
    NEXT

    NumPlayers = VAL(Humans$) + VAL(Computers$)

    IF NumPlayers = 0 THEN
      CLS
      PRINT "So I you don't want to play after all!"
      END
    END IF

    LOCATE 8, 21: PRINT "Skill level (1 to 100)"
    LOCATE 9, 22: PRINT "1   = Novice"
    LOCATE 10, 22: PRINT "90  = Expert"
    LOCATE 11, 22: PRINT "100 = Twiddle Fingers"
    LOCATE 12, 15: PRINT "(Computer speed may affect your skill level)"
    DO
        LOCATE 8, 44: PRINT SPACE$(35);
        LOCATE 8, 43
        INPUT gamespeed$
    LOOP UNTIL VAL(gamespeed$) >= 1 AND VAL(gamespeed$) <= 100
    speed = VAL(gamespeed$)

    speed = (100 - speed) * 2 + 1

    DO
        LOCATE 15, 56: PRINT SPACE$(25);
        LOCATE 15, 15
        INPUT "Increase game speed during play (Y or N)"; diff$
        diff$ = UCASE$(diff$)
    LOOP UNTIL diff$ = "Y" OR diff$ = "N"

    DO
      LOCATE 18: PRINT SPACE$(80)
      LOCATE 18, 15
      INPUT "How Many Lives Do You Want To Begin With"; BeginLives$
    LOOP UNTIL VAL(BeginLives$) > 0 AND VAL(BeginLives$) <= 15

    BeginLives = VAL(BeginLives$)

    DO
      LOCATE 20, 46
      LOCATE 20, 15
      INPUT "What Level Do You Want To Start At"; StartLevel$
    LOOP UNTIL VAL(StartLevel$) > 0

    StartLevel = VAL(StartLevel$)

    monitor = c
END SUB

'Significantly modified PlayNibbles function to accomodate the AI players
SUB PlayNibbles (NumPlayers, speed, diff$, BeginLives, StartLevel, Controllers(), Demo)
    DIM NumberCol AS INTEGER
    DIM NumberRow AS INTEGER
    DIM Winner AS INTEGER
    'Initialize Snakes
    DIM PeteBody(MAXSNAKELENGTH - 1, 1 TO MAXPLAYERS) AS snakeBody
    DIM Pete(1 TO MAXPLAYERS) AS SnakeType

    FOR I = 1 TO MAXPLAYERS
      Pete(I).score = 0
      Pete(I).lives = BeginLives
      Pete(I).CompControlled = Controllers(I)
    NEXT

    Pete(1).scolor = colorTable(1)
    Pete(2).scolor = colorTable(2)
    Pete(3).scolor = colorTable(7)

    IF Pete(1).CompControlled = TRUE THEN
      IF NumPlayers > 1 THEN
         Pete(1).title = "  Comp"
      ELSE
         Pete(1).title = "Pepper"
      END IF
    ELSE
      Pete(1).title = " Peter"
    END IF

    IF Pete(2).CompControlled = TRUE THEN
      IF NumPlayers > 2 THEN
         Pete(2).title = "   CPU"
      ELSE
         Pete(2).title = "Pepper"
      END IF
    ELSE
      Pete(2).title = " David"
    END IF

    Pete(3).title = "Pepper"

    Pete(1).TypeOfSnake = EATER
    Pete(2).TypeOfSnake = EATER
    Pete(3).TypeOfSnake = EATER

    gameOver = FALSE
    curSpeed = speed

    Level STARTOVER, Pete(), StartLevel
    SpacePause "     Level" + STR$(curLevel) + ",  Push Space"
         
    FOR I = NumPlayers + 1 TO MAXPLAYERS
      Pete(I).Row = 0
      Pete(I).Col = 0
      Pete(I).Direction = 0
    NEXT

    DO
        number = 1          'Current number that snakes are trying to run into
        nonum = TRUE        'nonum = TRUE if a number is not on the screen

        playerDied = FALSE
        PrintScore NumPlayers, Pete()
        PLAY "T160O1>L20CDEDCDL10ECC"

        DO
            'Print number if no number exists
            IF nonum = TRUE THEN
                DO
                    NumberRow = INT(RND(1) * 47 + 3)
                    NumberCol = INT(RND(1) * 78 + 2)
                    sisterRow = NumberRow + arena(NumberRow, NumberCol).sister
                LOOP UNTIL NOT PointIsThere(NumberRow, NumberCol, colorTable(4)) AND NOT PointIsThere(sisterRow, NumberCol, colorTable(4))
                NumberRow = arena(NumberRow, NumberCol).realRow
                nonum = FALSE
                COLOR colorTable(1), colorTable(4)
                LOCATE NumberRow, NumberCol
                PRINT RIGHT$(STR$(number), 1);
                count = 0
            END IF

            'Delay game
            FOR A# = 1 TO curSpeed:  NEXT A#

            'Get keyboard input & Change direction accordingly
            kbd$ = INKEY$
            SELECT CASE kbd$
                CASE "w", "W"
                  IF Pete(2).CompControlled = FALSE THEN
                     IF Pete(2).Direction <> 2 THEN Pete(2).Direction = 1
                  END IF
                CASE "s", "S"
                  IF Pete(2).CompControlled = FALSE THEN
                     IF Pete(2).Direction <> 1 THEN Pete(2).Direction = 2
                  END IF
                CASE "a", "A"
                  IF Pete(2).CompControlled = FALSE THEN
                     IF Pete(2).Direction <> 4 THEN Pete(2).Direction = 3
                  END IF
                CASE "d", "D"
                  IF Pete(2).CompControlled = FALSE THEN
                     IF Pete(2).Direction <> 3 THEN Pete(2).Direction = 4
                  END IF
                CASE CHR$(0) + "H"
                  IF Pete(1).CompControlled = FALSE THEN
                     IF Pete(1).Direction <> 2 THEN Pete(1).Direction = 1
                  END IF
                CASE CHR$(0) + "P"
                  IF Pete(1).CompControlled = FALSE THEN
                     IF Pete(1).Direction <> 1 THEN Pete(1).Direction = 2
                  END IF
                CASE CHR$(0) + "K"
                  IF Pete(1).CompControlled = FALSE THEN
                     IF Pete(1).Direction <> 4 THEN Pete(1).Direction = 3
                  END IF
                CASE CHR$(0) + "M"
                  IF Pete(1).CompControlled = FALSE THEN
                     IF Pete(1).Direction <> 3 THEN Pete(1).Direction = 4
                  END IF
                CASE "p", "P": SpacePause " Game Paused ... Push Space  "
                CASE " ": RandomWall
                CASE ELSE
            END SELECT

            FOR B = 1 TO NumPlayers
               IF Pete(B).CompControlled = TRUE THEN
                  Pete(B).Direction = NextSnakeMove(Pete(), NumberRow, NumberCol, B)
               END IF
            NEXT

            FOR A = 1 TO NumPlayers
                'Move Snake
                SELECT CASE Pete(A).Direction
                    CASE 1: Pete(A).Row = Pete(A).Row - 1
                    CASE 2: Pete(A).Row = Pete(A).Row + 1
                    CASE 3: Pete(A).Col = Pete(A).Col - 1
                    CASE 4: Pete(A).Col = Pete(A).Col + 1
                END SELECT

                IF DEBUG = TRUE THEN
                  'LOCATE 2, 77: PRINT "Pete("; A; ").Direction ="; Pete(A).Direction
                  LOCATE 24, 2: PRINT "TRUE Pete(2).Row"; Pete(2).Direction; "TRUE Pete(2).Col"; Pete(2).Col
                END IF

                LOCATE 1

                'If snake hits number, respond accordingly
                IF NumberRow = INT((Pete(A).Row + 1) / 2) AND NumberCol = Pete(A).Col THEN
                    PLAY "MBO0L16>CCCE"
                    IF Pete(A).Length < (MAXSNAKELENGTH - 30) THEN
                        Pete(A).Length = Pete(A).Length + number * 4
                    END IF

                    Pete(A).score = Pete(A).score + number
                    PrintScore NumPlayers, Pete()
                    number = number + 1
                    IF number = 10 THEN
                        FOR c = 1 TO NumPlayers
                           EraseSnake Pete(), PeteBody(), c
                        NEXT

                        LOCATE NumberRow, NumberCol: PRINT " "
                        Level NEXTLEVEL, Pete(), StartLevel
                        PrintScore NumPlayers, Pete()
                        IF Demo = FALSE THEN
                           SpacePause "     Level" + STR$(curLevel) + ",  Push Space"
                        END IF
                        FOR I = NumPlayers + 1 TO MAXPLAYERS
                           Pete(I).Row = 0
                        NEXT
                   
                        number = 1
                        IF diff$ = "Y" THEN speed = speed - 10: curSpeed = speed
                    END IF
                    nonum = TRUE
                    IF curSpeed < 1 THEN curSpeed = 1
                END IF
            NEXT A

            FOR A = 1 TO NumPlayers

                'If player runs into any point, or the head of the other snake, it dies.
                IF PointIsThere(Pete(A).Row, Pete(A).Col, colorTable(4)) OR (Pete(1).Row = Pete(2).Row AND Pete(1).Col = Pete(2).Col) THEN
                    PLAY "MBO0L32EFGEFDC"
                    COLOR , colorTable(4)
                    LOCATE NumberRow, NumberCol
                    PRINT " "

                    playerDied = TRUE
                    Pete(A).alive = FALSE
                    Pete(A).lives = Pete(A).lives - 1

                'Otherwise, move the snake, and erase the tail
                ELSE
                    Pete(A).head = (Pete(A).head + 1) MOD MAXSNAKELENGTH
                    PeteBody(Pete(A).head, A).Row = Pete(A).Row
                    PeteBody(Pete(A).head, A).Col = Pete(A).Col
                    tail = (Pete(A).head + MAXSNAKELENGTH - Pete(A).Length) MOD MAXSNAKELENGTH
                    Set PeteBody(tail, A).Row, PeteBody(tail, A).Col, colorTable(4)
                    PeteBody(tail, A).Row = 0
                    Set Pete(A).Row, Pete(A).Col, Pete(A).scolor
                END IF
            NEXT A
        LOOP UNTIL playerDied

        curSpeed = speed                ' reset speed to initial value

        FOR A = 1 TO NumPlayers
            EraseSnake Pete(), PeteBody(), A

            'If dead, then erase snake in really cool way
            IF Pete(A).alive = FALSE THEN
                'Update score
                Pete(A).score = Pete(A).score - 10
                PrintScore NumPlayers, Pete()

                SpacePause Pete(A).title + " Dies! Push Space!"
            END IF
        NEXT A

        Level SAMELEVEL, Pete(), StartLevel
        PrintScore NumPlayers, Pete()

        FOR I = 1 TO NumPlayers
         IF Pete(I).lives = 0 THEN
            EXIT DO
         END IF
        NEXT
    LOOP

    Winner = 1

    FOR I = 1 TO NumPlayers
      IF Pete(I).score > Pete(Winner).score THEN
         Winner = I
      END IF
    NEXT

    COLOR colorTable(5), colorTable(6)
    Center 10, "���������������������������������"
    Center 11, "�       G A M E   O V E R       �"
    Center 12, "�                               �"
    Center 13, "�          " + Pete(Winner).title + " Wins!         �"
    Center 14, "�                               �"
    Center 15, "�      Play Again?   (Y/N)      �"
    Center 16, "���������������������������������"
END SUB

'Added this function
FUNCTION NextSnakeMove (Pete() AS SnakeType, NumberRow AS INTEGER, NumberCol AS INTEGER, A AS INTEGER) STATIC
   DIM OldDir AS INTEGER
   DIM FoodDir AS INTEGER
   DIM I AS INTEGER
   DIM DeadEnds AS INTEGER

   FoodDir = Pete(A).Direction
   SELECT CASE Pete(A).TypeOfSnake
   CASE KILLER
   CASE EATER
      TargetDir = GoToNumber(NumberRow, NumberCol, INT((Pete(A).Row + 1) / 2), Pete(A).Col, FoodDir)
   CASE BOTH
   CASE ELSE
   END SELECT

   EscapeDir = AvoidWalls(Pete(A).Row, Pete(A).Col, Pete(A).Direction, NumberRow, NumberCol, WallInTheWay)

   IF WallInTheWay THEN
      NextSnakeMove = EscapeDir
   ELSE
      EscapeDir = AvoidWalls(Pete(A).Row, Pete(A).Col, TargetDir, NumberRow, NumberCol, WallInTheWay)
      IF NOT WallInTheWay THEN
         NextSnakeMove = TargetDir
      ELSE
         NextSnakeMove = EscapeDir
      END IF
   END IF

   OldDir = Pete(A).Direction
END FUNCTION

'Here is the RandomWall function
DEFINT A-Z
SUB RandomWall
   Length = 6

   BeginWallRow = INT(RND(1) * 47 + 3)
   IF BeginWallRow > 43 THEN
      BeginWallRow = 42
   END IF
  
   BeginWallCol = INT(RND(1) * 78 + 2)
   IF BeginWallCol > 73 THEN
      BeginWallCol = 72
   END IF

   IF RND(1) < .5 THEN
      FOR I = 1 TO Length
         Set BeginWallRow + I, BeginWallCol + I, colorTable(3)
      NEXT I
   ELSE
      FOR I = 1 TO Length
         Set BeginWallRow + I, BeginWallCol, colorTable(3)
      NEXT I
   END IF

   LOCATE 1, 40
   'PLAY "T160O1L8af"

END SUB