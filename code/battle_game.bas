DECLARE SUB GetInput (Diff AS INTEGER, Speed AS INTEGER)
nDECLARE SUB GetInput (Diff AS INTEGER, Speed AS INTEGER)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'                                 Battle                                    '
'                                                                           '
'                is a game made by David A. Raffensperger                   '
'                                                                           '
'  In Battle you use the W, S, A, and D keys to move and the Space Bar to   '
' to shoot. use T and H to rotate your ship. But avoid the feard monsters!  '
'                                                                           '
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                     
TYPE enemyStatusType
    col AS INTEGER
    row AS INTEGER
    dir AS INTEGER
    died AS INTEGER
    armor AS INTEGER
END TYPE

DEFINT A-Z
DIM whatKeyWas AS STRING, kbd AS STRING, speedString AS STRING
DIM enemyStatus(1 TO 2500) AS enemyStatusType, delayNum AS SINGLE
DECLARE SUB Pause (delayNum AS SINGLE)
DECLARE SUB GetKey (whatKeyWas AS STRING)
DECLARE SUB Shoot (level AS INTEGER, enemyStatus() AS enemyStatusType, youRow AS INTEGER, youCol AS INTEGER, youFacing AS INTEGER, enemiesAlive AS INTEGER, score AS INTEGER, pointsGiven AS INTEGER)
DECLARE SUB Enemy (i AS INTEGER, enemyStatus() AS enemyStatusType, youRow AS INTEGER, youCol AS INTEGER)
DECLARE SUB Help ()
DECLARE SUB SpecialHelp2 ()
DECLARE SUB SpecialHelp1 ()
DECLARE SUB SaveGame (shields AS INTEGER, youCol AS INTEGER, youCol AS INTEGER, timeLeft AS INTEGER, pointsGiven AS INTEGER, youFacing AS INTEGER, enemiesAlive AS INTEGER, nemyStatus() AS enemyStatusType)
CONST TRUE = 1
CONST FALSE = 0
RANDOMIZE TIMER
VIEW PRINT 1 TO 25
SCREEN 0

FOR i = 2 TO 5
    enemyStatus(i).died = FALSE
    enemyStatus(i).row = i
    enemyStatus(i).dir = 1
    enemyStatus(i).col = i
    enemyStatus(i).armor = 5
NEXT

enemyStatus(1).row = 6
enemyStatus(1).col = 6
enemyStatus(1).dir = 1
enemyStatus(1).armor = 5
enemyStatus(1).died = FALSE

timeLeft = 1000
level = 1
youFacing = 1
enemiesAlive = 6
wantsToQuit = FALSE
ContinueWhile = TRUE
youDied = FALSE

CLS

COLOR 7
LOCATE 1, 50
PRINT "Press F1 for help"
COLOR 3
LOCATE 1, 28
PRINT "Battle by David"
CALL GetInput(Speed, Diff)

SELECT CASE Diff
    CASE 1
      youCol = 40
      youRow = 24
      shields = 6
    CASE 2
      youCol = 60
      youRow = 18
      shields = 4
    CASE 3
      youCol = 40
      youRow = 12
      shields = 2
END SELECT

CLS

ON TIMER(1) GOSUB lessPoints
TIMER ON

WHILE ContinueWhile = TRUE
  DO
    IF pointsGiven > 50 THEN pointsGiven = 50
    IF pointsGiven < 1 THEN pointsGiven = 1
    IF timeLeft < 0 THEN timeLeft = 0
    FOR g = 1 TO shields
        COLOR 2
        LOCATE 1, 80 - g
        PRINT CHR$(234)
    NEXT g
    COLOR 3
    LOCATE 1, 28
    PRINT "Battle by David"
    COLOR 7
    LOCATE 1, 50
    PRINT "Press F1 for help"
    COLOR 10
    LOCATE 1, 12
    PRINT score

    FOR i = 1 TO level * 5
      SELECT CASE enemyStatus(i).dir         'Changes the points of your enemy, depending
        CASE 1
          enemyStatus(i).row = enemyStatus(i).row - 1
          IF enemyStatus(i).col <= 2 THEN enemyStatus(i).col = 79
        CASE 2
          enemyStatus(i).row = enemyStatus(i).row + 1
          IF enemyStatus(i).col >= 80 THEN enemyStatus(i).col = 3
        CASE 3
          enemyStatus(i).col = enemyStatus(i).col - 1
          IF enemyStatus(i).row <= 1 THEN enemyStatus(i).row = 24
        CASE 4
          enemyStatus(i).col = enemyStatus(i).col + 1
          IF enemyStatus(i).row >= 25 THEN enemyStatus(i).row = 2
      END SELECT

      IF enemyStatus(i).died = TRUE THEN enemyStatus(i).dir = 0
      IF enemyStatus(i).armor = 0 THEN
        enemyStatus(i).died = TRUE
      END IF

      CALL Enemy(i, enemyStatus(), youRow, youCol)

      SELECT CASE enemyStatus(i).armor
        CASE 5: COLOR 12
        CASE 4: COLOR 4
        CASE 3: COLOR 5
        CASE 2: COLOR 7
        CASE 1: COLOR 15
      END SELECT

      IF enemyStatus(i).col <= 2 THEN enemyStatus(i).col = 79
      IF enemyStatus(i).col >= 80 THEN enemyStatus(i).col = 3
      IF enemyStatus(i).row >= 25 THEN enemyStatus(i).row = 2
      IF enemyStatus(i).row <= 1 THEN enemyStatus(i).row = 24

      IF enemyStatus(i).died = FALSE THEN 'Prints your enemy, if it is alive
        LOCATE enemyStatus(i).row, enemyStatus(i).col
        PRINT CHR$(157)
      ELSE
        COLOR 14
        LOCATE enemyStatus(i).row, enemyStatus(i).col
        PRINT CHR$(219)
      END IF

      IF youCol > 79 THEN youCol = 2
      IF youRow > 24 THEN youRow = 3
      IF youCol < 2 THEN youCol = 78
      IF youRow < 2 THEN youRow = 23
    
      COLOR 10

      SELECT CASE youFacing
        CASE 1
          LOCATE youRow, youCol 'Prints your ship
          PRINT CHR$(60)
        CASE 2
          LOCATE youRow, youCol 'Prints your ship
          PRINT CHR$(62)
        CASE 3
          LOCATE youRow, youCol 'Prints your ship
          PRINT CHR$(118)
        CASE 4
          LOCATE youRow, youCol 'Prints your ship
          PRINT CHR$(94)
      END SELECT

   
      IF Speed > 0 THEN
        FOR q = 0 TO Speed
          Pause (Speed / level)
        NEXT q
      END IF

      LOCATE youRow, youCol 'Clears your ship (prints a space where your ship was)
      PRINT " "             'because you have moved
     
      SELECT CASE enemyStatus(i).dir
        CASE 1
          LOCATE enemyStatus(i).row + 1, enemyStatus(i).col
          PRINT " "
        CASE 2
          LOCATE enemyStatus(i).row - 1, enemyStatus(i).col
          PRINT " "
        CASE 3
          LOCATE enemyStatus(i).row, enemyStatus(i).col + 1
          PRINT " "
        CASE 4
          LOCATE enemyStatus(i).row, enemyStatus(i).col - 1
          PRINT " "
      END SELECT

      SELECT CASE INKEY$
        CASE CHR$(0) + "H"
          youRow = youRow - 1
          IF youRow <= 2 THEN youRow = 24
        CASE CHR$(0) + "P"
          youRow = youRow + 1
          IF youRow >= 25 THEN youRow = 3
        CASE CHR$(0) + "K"
          youCol = youCol - 1
          IF youCol <= 1 THEN youCol = 79
        CASE CHR$(0) + "M"
          youCol = youCol + 1
          IF youCol >= 80 THEN youCol = 2
        CASE "t"
          youFacing = youFacing + 1
          IF youFacing > 4 THEN youFacing = 1
        CASE "h"
          youFacing = youFacing - 1
          IF youFacing < 1 THEN youFacing = 4
        CASE " "
          CALL Shoot(level, enemyStatus(), youRow, youCol, youFacing, enemiesAlive, score, pointsGiven)
        CASE CHR$(0) + ";"
          CALL Help
        CASE CHR$(27)
          wantsToQuit = TRUE
          EXIT DO
        CASE "j"
          CALL SaveGame(shields, youCol, youCol, timeLeft, pointsGiven, youFacing, enemiesAlive, enemyStatus())
      END SELECT

      IF enemyStatus(i).col = youCol THEN
        IF enemyStatus(i).row = youRow THEN
          shields = shields - 1
          enemyStatus(i).row = 12
          enemyStatus(i).col = 20
          enemyStatus(i).dir = 1
          COLOR 4
          LOCATE 3, 15
          PRINT "You have been bit by an enemy, press any key to continue"
          GetKey (whatKeyWas)
          CLS
          FOR count = 1 TO 29
              SOUND 3000 - count * 100, 1
          NEXT count
          SELECT CASE Diff
            CASE 1
              youCol = 78
              youRow = 23
            CASE 2
              youCol = 60
              youRow = 18
            CASE 3
              youCol = 40
              youRow = 12
          END SELECT
        END IF
      END IF
    NEXT i
  
    IF shields = 0 THEN
      CLS
      COLOR 4
      LOCATE 12, 30
      PRINT "You have died!"
      COLOR 7
      LOCATE 23, 1
      PRINT "Push any key to continue"
      COLOR 9
      WHILE INKEY$ <> ""
        LOCATE 1, 1
        PRINT TIME$
      WEND
      WHILE INKEY$ <> ""
        LOCATE 1, 1
        PRINT TIME$
      WEND
      EXIT DO
    END IF

    IF enemiesAlive <= 1 THEN
      CLS
      COLOR 2
      LOCATE 10, 30
      PRINT "You have won level"; level; ","
      LOCATE 11, 30
      PRINT "and your bonus is"; timeLeft
      COLOR 2
      LOCATE 24, 1
      PRINT "Press any to countinue"
      PLAY "effdcaa"
      score = score + timeLeft
      timeLeft = (level * 500) + 500
      pointsGiven = 0
      level = level + 1
      enemiesAlive = level * 5 + 1
      FOR count1 = 1 TO enemiesAlive - 1
        enemyStatus(count1).armor = 5
         enemyStatus(count1).died = FALSE
        enemyStatus(count1).dir = 1
      NEXT count1
      SELECT CASE Diff
        CASE 1
          youCol = 78
          youRow = 23
        CASE 2
          youCol = 60
          youRow = 18
        CASE 3
          youCol = 40
          youRow = 12
      END SELECT
      GetKey (whatKeyWas)
      CLS
    END IF
  LOOP
  IF shields = 0 THEN
     CLS
     COLOR 4
     LOCATE 12, 30
     PRINT "You have died!"
     COLOR 7
     LOCATE 14, 1
     PRINT "Do you want to play again? y/n"
  END IF
  DO
     kbd = INKEY$
     IF kbd = CHR$(0) + ";" THEN CALL Help
     IF UCASE$(kbd) = "N" THEN
        ContinueWhile = FALSE
        EXIT DO
     END IF
     IF UCASE$(kbd) = "Y" THEN
       youFacing = 1
       score = 0
       level = 1
       ContinueWhile = TRUE
       CLS
       CALL GetInput(Speed, Diff)
       SELECT CASE Diff
         CASE 1
           youCol = 78
           youRow = 23
         CASE 2
           youCol = 60
           youRow = 18
         CASE 3
           youCol = 40
           youRow = 12
       END SELECT
       FOR i = 2 TO 5
         enemyStatus(i).died = FALSE
         enemyStatus(i).row = i
         enemyStatus(i).dir = 1
         enemyStatus(i).col = i
         enemyStatus(i).armor = 5
       NEXT
       enemyStatus(1).row = 6
       enemyStatus(1).col = 6
       enemyStatus(1).dir = 1
       enemyStatus(1).armor = 5
       enemyStatus(1).died = FALSE
       EXIT DO
     END IF
  LOOP
WEND

lessPoints:
    pointsGiven = pointsGiven - 5
    timeLeft = timeLeft - 10
    COLOR 9
    LOCATE 1, 1
    PRINT TIME$
    END

CLS

DEFSNG A-Z
SUB Enemy (i AS INTEGER, enemyStatus() AS enemyStatusType, youRow AS INTEGER, youCol AS INTEGER)
IF youRow < enemyStatus(i).row THEN
    IF youCol < enemyStatus(i).col THEN
        SELECT CASE enemyStatus(i).dir
            CASE 2: enemyStatus(i).dir = 3
            CASE 4: enemyStatus(i).dir = 1
        END SELECT
    ELSE
        SELECT CASE enemyStatus(i).dir
            CASE 2: enemyStatus(i).dir = 4
            CASE 3: enemyStatus(i).dir = 1
        END SELECT
    END IF
ELSE
    IF youCol < enemyStatus(i).col THEN
        SELECT CASE enemyStatus(i).dir
            CASE 1: enemyStatus(i).dir = 3
            CASE 4: enemyStatus(i).dir = 2
        END SELECT
    ELSE
        SELECT CASE enemyStatus(i).dir
            CASE 1: enemyStatus(i).dir = 4
            CASE 3: enemyStatus(i).dir = 2
        END SELECT
    END IF
END IF

END SUB

DEFINT A-Z
SUB GetInput (Diff AS INTEGER, Speed AS INTEGER)

COLOR 7
LOCATE 1, 50
PRINT "Press F1 for help"
COLOR 3
LOCATE 1, 28
PRINT "Battle by David"
DIM speedString AS STRING
DIM kbd AS STRING
COLOR 7
LOCATE 3, 20
PRINT "Type in a number between 0 and 9,"
LOCATE 4, 17
PRINT "for how fast do you want the game to go"

COLOR 9
DO
  LOCATE 1, 1
  PRINT TIME$
  speedString = INKEY$
  IF speedString = CHR$(0) + ";" THEN CALL SpecialHelp1
  IF speedString <> CHR$(0) + ";" AND speedString <> "" THEN EXIT DO
LOOP

Speed = VAL(speedString)

COLOR 7
LOCATE 3
PRINT STRING$(80, " ")
LOCATE 4
PRINT STRING$(80, " ")
LOCATE 3, 25
PRINT "You want the game to be:"

DO
  kbd = INKEY$
 
  COLOR 9
  LOCATE 1, 1
  PRINT TIME$

  SELECT CASE kbd
    CASE CHR$(13): done = 1
    CASE CHR$(0) + "H": selected = selected + 1
    CASE CHR$(0) + "P": selected = selected - 1
    CASE CHR$(0) + "K": selected = selected + 1
    CASE CHR$(0) + "M": selected = selected - 1
    CASE " ": selected = selected + 1
    CASE CHR$(0) + ";": CALL SpecialHelp2
  END SELECT
      
  COLOR 7

  SELECT CASE selected
    CASE 1
      LOCATE 3, 50
      PRINT "Easy"
    CASE 2
      LOCATE 3, 50
      PRINT "Medium"
    CASE 3
      LOCATE 3, 50
      PRINT "Hard"
  END SELECT

  IF selected < 1 THEN selected = 3
  IF selected > 3 THEN selected = 1
  IF selected <> 2 THEN
    LOCATE 3, 54
    PRINT "  "
  END IF
LOOP UNTIL done = 1

Diff = selected

END SUB

DEFSNG A-Z
SUB GetKey (whatKeyWas AS STRING)

DIM kbd AS STRING

COLOR 9

DO
  kbd = INKEY$
LOOP UNTIL kbd <> ""

whatKeyWas = kbd

END SUB

DEFINT A-Z
SUB Help
  DIM kbd AS STRING
  COLOR 7
  CLS
  LOCATE 3, 5
  PRINT "To move your ship use the up, down, left, and right arrows to move."
  LOCATE 5, 25
  PRINT "To shoot, press the spacebar."
  LOCATE 7, 20
  PRINT "To rotate your ship ust the T and H keys."
  LOCATE 9, 25
  PRINT "To quit this game press Alt F4."
  DO
    kbd = INKEY$
  LOOP UNTIL kbd <> ""
  LOCATE 3
  PRINT SPC(80);
  LOCATE 5
  PRINT SPC(80);
  LOCATE 7
  PRINT SPC(80);
  LOCATE 9
  PRINT SPC(80);
END SUB

DEFSNG A-Z
SUB Pause (delayNum AS SINGLE)

DIM delay AS DOUBLE
DIM i AS DOUBLE
 
delay = 8.4678549097#
                               
FOR i = 1 TO delayNum STEP .1
  delay = delay + delay - delay
NEXT i

END SUB

DEFINT A-Z
SUB SaveGame (shields AS INTEGER, youCol AS INTEGER, youRow AS INTEGER, timeLeft AS INTEGER, pointsGiven AS INTEGER, youFacing AS INTEGER, enemiesAlive AS INTEGER, enemyStatus() AS enemyStatusType)

CLS
DIM nameFile AS STRING
INPUT "What game do you want to load"; nameFile
OPEN nameFile FOR OUTPUT AS #1
WRITE #1, shields, youCol, youRow, score, pointsGiven, timeLeft, youFacing
FOR i = 1 TO enemiesAlive - 1
    WRITE #1, enemyStatus(i).row
NEXT i
FOR i = 1 TO enemiesAlive - 1
    WRITE #1, enemyStatus(i).dir
NEXT i
FOR i = 1 TO enemiesAlive - 1
    WRITE #1, enemyStatus(i).armor
NEXT i
FOR i = 1 TO enemiesAlive - 1
    WRITE #1, enemyStatus(i).col
NEXT i

    CLOSE #1
    'Echo the file back.
    'OPEN "LIST" FOR INPUT AS #1
    'CLS
    'PRINT "Entries in file:": PRINT
    'DO WHILE NOT EOF(1)
    '    LINE INPUT #1, REC$  'Read entries from the file.
    '    PRINT REC$           'Print the entries on the screen.
    'LOOP
    'CLOSE #1
    'KILL "LIST"
END SUB

DEFSNG A-Z
SUB Shoot (level AS INTEGER, enemyStatus() AS enemyStatusType, youRow AS INTEGER, youCol AS INTEGER, youFacing AS INTEGER, enemiesAlive AS INTEGER, score AS INTEGER, pointsGiven AS INTEGER)
'left right down up
  COLOR 9
  FOR count = 1 TO 29
       SOUND 3000 - count * 100, .2
  NEXT count

  SELECT CASE youFacing
      CASE 3

        FOR a = youRow TO 25
          COLOR 2
          LOCATE a, youCol
          PRINT CHR$(179)
        NEXT a

        FOR i = 1 TO level * 5
          IF enemyStatus(i).row > youRow THEN
            IF enemyStatus(i).col = youCol THEN
              enemyStatus(i).armor = enemyStatus(i).armor - 1
              score = score + (50 - pointsGiven)
              IF enemyStatus(i).armor = 0 THEN enemiesAlive = enemiesAlive - 1
                SOUND 3500, .3
              END IF
          END IF
        NEXT

    CASE 2
      FOR a = youCol TO 80
        LOCATE youRow, a
        PRINT CHR$(196)
      NEXT a

        FOR i = 1 TO level * 5
            IF youCol < enemyStatus(i).col THEN
                IF youRow = enemyStatus(i).row THEN
                    IF enemyStatus(i).died = FALSE THEN
                        enemyStatus(i).armor = enemyStatus(i).armor - 1
                        score = score + (50 - pointsGiven)
                        IF enemyStatus(i).armor = 0 THEN enemiesAlive = enemiesAlive - 1
                        SOUND 3500, .3
                    END IF
                END IF
             END IF
        NEXT i
     
    CASE 1
      FOR a = youCol TO 1 STEP -1
          COLOR 2
          LOCATE youRow, a
          PRINT CHR$(196)
      NEXT a

       FOR i = 1 TO level * 5
            IF enemyStatus(i).col < youCol THEN
                IF youRow = enemyStatus(i).row THEN
                    IF enemyStatus(i).died = FALSE THEN
                        enemyStatus(i).armor = enemyStatus(i).armor - 1
                        score = score + (50 - pointsGiven)
                        IF enemyStatus(i).armor = 0 THEN enemiesAlive = enemiesAlive - 1
                        SOUND 3500, .3
                    END IF
                END IF
            END IF
      NEXT

    CASE 4

      FOR a = youRow TO 1 STEP -1
          COLOR 2
          LOCATE a, youCol
          PRINT CHR$(179)
      NEXT
     
          FOR i = 1 TO level * 5
            IF enemyStatus(i).row < youRow THEN
                IF youCol = enemyStatus(i).col THEN
                    IF enemyStatus(i).died = FALSE THEN
                        enemyStatus(i).armor = enemyStatus(i).armor - 1
                        score = score + (50 - pointsGiven)
                        IF enemyStatus(i).armor = 0 THEN enemiesAlive = enemiesAlive - 1
                        SOUND 3500, .3
                    END IF
                END IF
             END IF
          NEXT i

  END SELECT

  CLS

END SUB

DEFINT A-Z
SUB SpecialHelp1
    CLS
    COLOR 7
    LOCATE 3, 5
    PRINT "Before you really start playing, I recomend you try out a few of"
    LOCATE 4, 10
    PRINT "the speeds before you decide witch one you like best."
    LOCATE 6, 25
    PRINT "The speed of your computer can"
    LOCATE 7, 20
    PRINT "also effect the speed you should play at."
    DO
    LOOP WHILE INKEY$ = ""
    CLS
    COLOR 3
    LOCATE 1, 28
    PRINT "Battle by David"
    COLOR 7
    LOCATE 1, 50
    PRINT "Press F1 for help"
    LOCATE 3, 20
    PRINT "Type in a number between 0 and 9,"
    LOCATE 4, 17
    PRINT "for how fast do you want the game to go"
    COLOR 9
END SUB

SUB SpecialHelp2
    CLS
    COLOR 7
    LOCATE 3, 15
    PRINT "On Easy mode you get 6 shields, and start on a corner."
    LOCATE 5, 15
    PRINT "On Medium mode you get 4 shields, and start in betwee the corner and the middle."
    LOCATE 8, 15
    PRINT "On Hard mode you get 2 shields, and start in the middle."
    LOCATE 10, 20
    PRINT "NOTE: if you were on Easy mode  final score will only be 1/3 of what it would be on Hard. On Medium mode it would only be 1/2 of what it would be on Hard."
    COLOR 9
    DO
       LOCATE 1, 1
       PRINT TIME$
    LOOP WHILE INKEY$ = ""
    CLS
    COLOR 3
    LOCATE 1, 28
    PRINT "Battle by David"
    COLOR 7
    LOCATE 3, 25
    PRINT "You want the game to be:"
    LOCATE 1, 50
    PRINT "Press F1 for help"
END SUB

