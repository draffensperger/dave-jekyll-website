HOW TO PLAY MAZEGAME

In Mazegame your objective is to find a secret 
treasure chest.  However, this is no easy task; 
there are trap floors with lava below, color-
coded locks and keys, and then there's the sight
of sky above which is a painful reminder of 
freedom.

Click on doors to go to other rooms (and arrows
which point backward are doors). If a door 
has a lock on it the key that matches the color
of that lock will unlick it. If a key is on the
floor, click to pick it up.